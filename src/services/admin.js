import ApiService from './base';

export const registerNewAdmin = async (data) => {
  let uid = getUid()
  const url = `/registerNewAdmin`
  return ApiService.post(url, {...data, uid})
}

export const getAllAdmin = async () => {
  let uid = getUid()
  const url = `/getAllAdmin`
  return ApiService.post(url, {uid})
}

export const deleteAdmin = async (adminid) => {
  let uid = getUid()
  const url = `/deleteAdmin`
  return ApiService.post(url, {adminid, uid})
}

export const getAdminData = async (adminid) => {
  let uid = getUid()
  const url = `/getAdminData`
  return ApiService.post(url, {adminid, uid})
}

export const updateAdminName = async (data) => {
  let uid = getUid()
  const url = `/updateAdminName`
  return ApiService.post(url, {...data, uid})
}

export const updateAdminPassword = async (data) => {
  let uid = getUid()
  const url = `/updateAdminPassword`
  return ApiService.post(url, {...data, uid})
}

const getUid = () => {
  let uid = null;
  try{
    const token = JSON.parse(localStorage.getItem("TOKEN")) || {};
    uid = token.localId || null;
  }catch(e){console(e)}
  return uid;
}