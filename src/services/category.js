import ApiService from './base';

export const getAllCategory = async () => {
  const url = `/getAllCategory`
  return ApiService.post(url)
}

export const createCategory = async (data) => {
  const url = `/createCategory`
  return ApiService.post(url, data)
}

export const updateCategory = async (data) => {
  const url = `/updateCategory`
  return ApiService.post(url, data)
}

export const deleteCategory = async (id) => {
  const url = `/deleteCategory`
  return ApiService.post(url, {id})
}