import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import 'bootstrap'
import 'bootstrap/dist/css/bootstrap.min.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

import Loading from 'vue-loading-overlay';
import 'vue-loading-overlay/dist/vue-loading.css';

import VueToast from 'vue-toast-notification';
import 'vue-toast-notification/dist/theme-default.css';

import Vuelidate from 'vuelidate';

import  ApiService  from '@/services/base';

import * as firebase from 'firebase';

const FBCONFIG = {
  apiKey: "AIzaSyA7Qn7AWRZyr_VEzeoDej2nG1hQPbk5in4",
  authDomain: "tea24hstore.firebaseapp.com",
  databaseURL: "https://tea24hstore.firebaseio.com",
  projectId: "tea24hstore",
  storageBucket: "tea24hstore.appspot.com",
  messagingSenderId: "468573818462",
  appId: "1:468573818462:web:c10553668aa73cd18b2ecf",
  measurementId: "G-ECB2WKW04W"
};

firebase.initializeApp(FBCONFIG);

Vue.config.productionTip = false;

Vue.use(Vuelidate)

Vue.use(VueToast, {
  position: 'top',
  duration: 2 * 1000
})

Vue.use(Loading, {color: '#1428A0'}) // Primary color

ApiService.init();

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')

