import Vue from 'vue'
import VueRouter from 'vue-router'
import ListUser from '../components/Layouts/User/ListUser.vue'
import ListAdmin from '../components/Layouts/Admin/ListAdmin.vue'
import AddAdmin from '../components/Layouts/Admin/AddAdmin.vue'
import EditAdmin from '../components/Layouts/Admin/EditAdmin.vue'
import CategoryList from '../components/Layouts/Category/List.vue'
import ListProduct from '../components/Layouts/Product/ListProduct.vue'
import AddProduct from '../components/Layouts/Product/AddProduct.vue'
import EditProduct from '../components/Layouts/Product/EditProduct.vue'
import ListOrder from '../components/Layouts/ListOrder.vue'
import DetailOrder from '../components/Layouts/DetailOrder.vue'
import LoginAdmin from '../components/Login.vue'
import Layout from './../views/Layout.vue';

Vue.use(VueRouter)

  const routes = [
    {
      path: '/login',
      name: 'login',
      component: LoginAdmin
    },
    {
      path: '/',
      redirect: '/category'
    },
    {
      path: '/',
      name: 'admin',
      component: Layout,
      redirect: '/category',
      children: [
        {
          path: '/list-user',
          name: 'listuser',
          component: ListUser
        },
        {
          path: '/category',
          name: 'category',
          component: CategoryList
        },
        {
          path: '/list-product',
          name: 'listproduct',
          component: ListProduct
        },
        {
          path: '/list-product/create',
          name: 'createproduct',
          component: AddProduct
        },
        {
          path: '/list-product/:id',
          name: 'editproduct',
          component: EditProduct
        },
        {
          path: '/list-admin',
          name: 'listadmin',
          component: ListAdmin
        },
        {
          path: '/list-admin/create',
          name: 'createadmin',
          component: AddAdmin
        },
        {
          path: '/list-admin/:id',
          name: 'editadmin',
          component: EditAdmin
        },
        {
          path: '/list-order',
          name: 'listorder',
          component: ListOrder
        },
        {
          path: '/detail-order/:id',
          name: 'detailorder',
          component: DetailOrder
        },
      ]
    },
  ]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

console.log(router.beforeEach)

router.beforeEach((to, from, next) => {

  if(to.name !== "login"){
    let token = null;
    try{
      token = JSON.parse(localStorage.getItem("TOKEN"))
    }catch(e){console.log(e)}
    if(!token){
      return  next('/login')
    }
  }
  next() 
});

export default router
