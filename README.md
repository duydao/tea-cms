# prj_tea_cms

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).


### PUSH code
git fetch
git pull

git checkout -b Branch_name
git add .
git commit -m "name commit"
git push